package com.example.demo.services;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author karkoub
 * @implNote Implements the business layer of the application 
 */

@Service
public class FileSystemStorageService {
	
	
	private Path uploadLocation;
	private Path uploadLocation2;

	//Get the secret Key from application.properties file
	@Value("${secretKey}")
	private String secretKey;
	
	
	
	//Initialize the paths where to upload the encrypted and decrypted files
	@PostConstruct
	public void init() {
		
		this.uploadLocation = Paths.get(Constants.UPLOAD_LOCATION);
		this.uploadLocation2 = Paths.get(Constants.UPLOAD_LOCATION2);

		try {
			Files.createDirectories(uploadLocation);
			Files.createDirectories(uploadLocation2);

		} catch (IOException e) {
			throw new RuntimeException("Could not initialize storage", e);
		}
	}
	
	//Encrypt and Upload the encrypted file using AES Algorithm
	public void store(MultipartFile file) {
		
		  init();

		  File inputFile = new File(file.getOriginalFilename());
		  String filename1 = inputFile.getName();
		  String filename2 = filename1.substring(0, filename1.indexOf('.'));
		  
		  //Create the new encrypted file
		  File encryptedFile = new File(filename2+ "_encrypted.txt");
		  

		  String filename = StringUtils.cleanPath(file.getOriginalFilename());
		
	
		try {
			if (file.isEmpty()) {
				throw new RuntimeException("Failed to store empty file " + filename);
			}
			
			// This is a security check
			if (filename.contains("..")) {
				throw new RuntimeException("Cannot store file with relative path outside current directory " + filename);
			}
			
			
			       //Read the content of the file using java.io package (Streams, BufferReader...) 
				   InputStream inputStream =  file.getInputStream();
				   BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
					 StringBuilder sb = new StringBuilder();
				        String line;
				        while ((line = br.readLine()) != null) {
				            sb.append(line + System.lineSeparator());
				        }
				        
				        
                   //Encrypt the content of the file
					   byte[] outputBytes = AESAlgorithm.encrypt(sb.toString(),secretKey ).getBytes();
				   FileOutputStream outputStream = new FileOutputStream(encryptedFile);
				   outputStream.write(outputBytes);
				   
				   
				//Upload the encrypted file			
				Files.copy(encryptedFile.toPath(), this.uploadLocation.resolve(encryptedFile.getName()), StandardCopyOption.REPLACE_EXISTING);
			
				 inputStream.close();
				 outputStream.close();
				  }
			
		    catch (IOException e) {
			throw new RuntimeException("Failed to store file " + filename, e);
		}
	
	}

	
	
	//Decrypt a selected file using the AES algorithm and download it
	public String DecryptFile (String fileName) throws IOException 
	
	{ 
		
		//Read the file by his name
		File file = new File("C:\\java-exec\\upload-dir\\encrypted-files\\"+ fileName);
		
	     
	  
		try {
		if (file.isFile()) {
			
			//Read the content of the file using java.io package (Streams, BufferReader...) 
			 InputStream inputStream =  new FileInputStream(file);
			 BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
			 StringBuilder sb = new StringBuilder();
		        String line;
		        while ((line = br.readLine()) != null) {
		            sb.append(line);
		        }
		        
		        
               //Decrypt the file using AES algorithm
			   byte[] outputBytes = AESAlgorithm.decrypt(sb.toString(),secretKey ).getBytes();
			   
			   //Generate the new decrypted file
			   String decFileName = fileName.substring(0,fileName.indexOf('_'));

			   File decryptedFile = new File(decFileName+ "_decrypted.txt");

			   FileOutputStream outputStream = new FileOutputStream(decryptedFile);
			   outputStream.write(outputBytes);
			   
			   
			//Upload the decrypted file			
			Files.copy(decryptedFile.toPath(), this.uploadLocation2.resolve(decryptedFile.getName()), StandardCopyOption.REPLACE_EXISTING);
		
			 inputStream.close();
			 outputStream.close();

				return(decryptedFile.getName());

			  
		} else {
			throw new RuntimeException("Could not read file: " + fileName);
		}
	} catch (MalformedURLException e) {
		throw new RuntimeException("Could not read file: " + fileName, e);
	}
		
}
	
	// Download the decrypted file
	public Resource loadDecResource(String filename) {

	
	Path file2 = uploadLocation2.resolve(filename);
	Resource resource;
	try {
		resource = new UrlResource(file2.toUri());
	 
	if (resource.exists() || resource.isReadable()) {
		return resource;
	} else {
		throw new RuntimeException("Could not read file: " + filename);
	}
	}catch (MalformedURLException e) {
		throw new RuntimeException("Could not read file: " + filename, e);

	}
	}
	
	//Download the encrypted file
	public Resource loadEncResource(String filename) {
		try {
			Path file = uploadLocation.resolve(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new RuntimeException("Could not read file: " + filename);
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("Could not read file: " + filename, e);
		}
	}
	

	//Return the list of the encrypted files
	public List<Path> listSourceEncryptedFiles(Path dir) throws IOException {
		List<Path> result = new ArrayList<>();
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "*.{txt}")) {
			for (Path entry : stream) {
				result.add(entry);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	
	//Return the path of the encrypted files
	public Path getUploadLocation() {
		return uploadLocation;
	}
	
	
}