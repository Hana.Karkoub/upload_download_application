package com.example.demo.services;

/**
 * 
 * @author karkoub
 *
 */

//Declaration of paths as static variables
public class Constants {
	
	//Path for encrypted files
	public static final String UPLOAD_LOCATION = "C:\\java-exec\\upload-dir\\encrypted-files\\";
	
	//Path for decrypted files
	public static final String UPLOAD_LOCATION2 = "C:\\java-exec\\upload-dir\\decrypted-files\\";

}
