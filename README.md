# Upload_Download_Application

A sample user interface for uploading/downloading files using the symetric AES algorithm to encryption/decryption operations.


## Build With
* JAVA 8
* [Spring Boot 2.5.3](https://projects.spring.io/spring-boot)
* [Maven 4.0.0](https://maven.apache.org) - Dependency Management
* HTML5/CSS3 
* Thymeleaf 
* jquery 2.1.4
* bootstrap 3.3.6

## Design Pattern 

This project follows the Model-View-Controller design pattern.

* Model - A model contains a single object.
* Controller - the @Controller annotation is used to mark the class as the controller which contains the business logic of the application.
* View -  HTML5 templates is used to create a view page supported by the Thymeleaf technologie provided by spring Framework.


## Steps to Setup

**1. Clone the repository** 
```bash
git clone https://gitlab.com/Hana.Karkoub/upload_download_application.git
```

**2. Run the app using maven**

```bash
cd upload_download_application
mvn spring-boot:run
```

Or you may also run the application manually:

* Open the project "upload_download_application" in a specific IDE ( Eclipse ...)
* Right Click on the class UploadDownloadFilesApplication.java 
* Click on run as 
* Choose java application

** Upload Paths: 
For encrypted files: C:\java-exec\upload-dir\encrypted-files\
For decrypted files: C:\java-exec\upload-dir\decrypted-files\

That's it! The application can be accessed at http://localhost:8081

<p align="center">
    <br>
    <br>
    <img width="800px" height="200px" src="https://gitlab.com/Hana.Karkoub/upload_download_application/-/blob/main/upload_download_application/demo.PNG">
</p>


* The solution could be further improved by adding other type of files besides .txt like (.xls, pdf, ppt ...).
